# setup initial list
import random

positive_list = ['happy', 'good', 'grateful', 'relaxed', 'amazing',
                 'astonished', 'surprised', 'ecstatic', 'excited', 'elated',
                 'delighted', 'glad', 'confident', 'pleased', 'lovestruck',
                 'hopeful', 'proud', 'inspired', 'contented', 'satisfied',
                 'optimistic', 'energetic', 'jubilant', 'motivated', 'refreshed',
                 'fantastic', 'great',]
neutral_list = ['fine', 'okay', 'neutral', 'calm']
negative_list = ['lonely', 'tired', 'exhausted', 'jealous', 'conflicted',
                 'confused', 'embarrassed', 'frustrated', 'depressed', 'sad',
                 'insecure', 'stressed', 'angry', 'anxious', 'drained',
                 'pessimistic', 'guilty', 'frightened', 'bored', 'hurt',
                 'disappointed', 'discontented', 'unsatisfied', 'ashamed',
                 'upset', 'nervous', 'lonely', 'hopeless', 'heartbroken',
                 'regretful', 'annoyed', 'worried', 'unhappy', 'shocked',
                 'scared', 'betrayed', 'dejected', 'disgusted']

emotion_list = positive_list + neutral_list + negative_list

positive_suggestion = ["Great! Sing and dance away to some good music: www.tinyurl.com/happy-hits",
                       "You’re a champ! Shop online and buy yourself something you’ve always wanted!",
                       "Time to marathon your favorite show on Netflix!",
                       "Unwind and play some onlie games!",
                       "Sweet! Throw a big party to celebrate!",
                       "Watch all the cute animal videos on YouTube you want.",
                       "Treat yourself to some ice cream.",
                       "You had a long day,  enjoy a bubble bath or a long shower.",
                       "That's great! Write it in your journal.",
                       "Enjoy!",
                       "Well done. Go relax!"]
neutral_suggestion = ["Go marathon your favorite show on Netflix!",
                      "Go call a friend or family member who makes you smile.",
                      "Go do something fun!",
                      "You should try midnight baking."]
negative_suggestion = ["Life is like an onion; you peel it off one layer at a time, and sometimes you weep.",
                       "Give yourself some space, and some time to yourself.",
                       "Try to breathe deeply when you feel yourself getting upset.",
                       "Anger can be a positive force in your life, or it can be exhausting and destructive... And sometimes it's hard to tell the difference!",
                       "It seems like life is asking so much of you now. I want you to plan some time just for yourself, to do anything that will make you feel happier.",
                       "I’m here for you. I want you to feel safe. Please get help here: www.suicidepreventionlifeline.org",
                       "Sometimes stress is an indication that you’re trying to do too much.",
                       "If I were there, I’d give you a hug.",
                       "When the going gets tough, put one foot in front of the other and just keep going. Don’t give up.",
                       "When my nerves feel raw, I find that focusing on my breathing can really help. Here’s a guided breathing exercise: https://youtu.be/SEfs5TJZ6Nk",
                       "I think meditating is a good way to witness yourself.",
                       "You’re going to do great, I know it!",
                       "You don’t need to do anything, because if you see yourself in the correct way, you are all as much extraordinary phenomenon of nature as trees, clouds, the patterns in running water, the flickering of fire, the arrangement of the stars, and the form of a galaxy. You are all just like that, and there is nothing wrong with you at all.",
                       "I wish I could do something to make you feel better.",
                       "I’m sorry that you had to go through that.",
                       "I really hope you’ll be okay.",
                       "Let’s focus on gratitude. What’s the last thing that made you feel grateful?",
                       "Don’t worry."]

# user input
def get_response(user_input):
    if str(user_input.lower()) in emotion_list:
        return str(user_input.lower()) #true or false

# check input for keyword
def response_handler1(emotional_state):
    
    if emotional_state in positive_list:
        return input("That’s good to hear. What made you feel {}?\n".format(emotional_state))
    elif emotional_state in neutral_list:
        return "Well, I’m glad you’re doing {}.\n".format(emotional_state)
    elif emotional_state in negative_list:
        return input("Oh no… What made you so {}?\n".format(emotional_state))
    else:
        return input("I'm sorry, I don't quite understand that.\n")

# check input for suggestion
def response_handler2(emotional_state):
    if emotional_state in positive_list:
        return input(random.choice(positive_suggestion)+'\n')
    elif emotional_state in neutral_list:
        return input(random.choice(neutral_suggestion)+'\n')
    elif emotional_state in negative_list:
        return input(random.choice(negative_suggestion)+'\n')
    else:
        return input("Okay.\n")

    # manipulate + update w response
def chatbot_logic():
    counter = 1
    emotion  = None 
    name = input("Hello, what is your name?\n")
    user_input = input("Hi " + name + "! How are you feeling today?\n")
    
    while counter <=4:
        if counter <2: 
            emotional_state = (get_response(user_input))
            emotion = emotional_state  
            response = response_handler1(emotional_state)
            counter +=1
        elif counter == 2:
            emotional_state = (get_response(user_input))
            response = response_handler2(emotion)
            counter  +=1
        elif counter == 3:
            input_string = input("How can you make tomorrow better?\n")
            counter +=1
        elif  counter == 4:
            input_string = input("Sounds good. I'm sure it will be. Goodnight :)\n")
            break
    return input("Bye!")

chatbot_logic()
